#ifndef P3DOS_INC
P3DOS_INC = 1

IDE_STREAM_OPEN 	=$0056 ; 
IDE_STREAM_CLOSE	=$0059 ; 
IDE_STREAM_IN	=$005c ; 
IDE_STREAM_OUT	=$005f ; 
IDE_STREAM_PTR	=$0062 ; 
IDE_BANK		=$01bd ; 
IDE_BASIC 		=$01c0 ; 
IDE_WINDOW_LINEIN 	=$01c3 ; 
IDE_WINDOW_STRING 	=$01c6 ; 
IDE_INTEGER_VAR 	=$01c9 ; 
IDE_RTC 		=$01cc ; 
IDE_DRIVER 		=$01cf ; 
IDE_MOUNT 		=$01d2 ; 
IDE_MODE 		=$01d5 ; 
IDE_TOKENISER 	=$01d8 ; 


RC_BANKTYPE_ZX 	= 0 ; ZX memory half-banks (8K size)
RC_BANKTYPE_MMC	= 1 ; DivMMC memory banks (8K size)

RC_BANK_TOTAL 	= 0 ; return total number of 8K banks of specified type
RC_BANK_ALLOC 	= 1 ; allocate next available 8K bank
RC_BANK_RESERVE 	= 2 ; reserve bank specified in E (0..total-1) 
RC_BANK_FREE 	= 3 ; free bank specified in E (0..total-1) 
RC_BANK_AVAILABLE 	= 4 ; return number of currently-available 8K banks

#endif