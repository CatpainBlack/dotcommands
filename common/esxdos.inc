#ifndef ESXDOS_INC
ESXDOS_INC = 1

;Low-level calls
DISK_FILEMAP	= $85 ;obtain file allocation map
DISK_STRMSTART	= $86 ;start streaming operation
DISK_STRMEND	= $87 ;end streaming operation

;Miscellaneous calls.
M_DOSVERSION	= $88 ;get NextZXOS version/mode information
M_GETSETDRV		= $89 ;get/set default drive
M_TAPEIN		= $8b ;tape redirection control (input)
M_TAPEOUT		= $8c ;tape redirection control (output)
M_GETHANDLE		= $8d ;get handle for current dot command
M_GETDATE		= $8e ;get current date/time
M_EXECCMD		= $8f ;execute a dot command
M_SETCAPS		= $91 ;set additional capabilities
M_DRVAPI		= $92 ;access API for installable drivers
M_GETERR		= $93 ;get or generate error message
M_P3DOS		= $94 ;execute +3DOS/IDEDOS/NextZXOS call
M_ERRH		= $95 ;register dot command error handler

; FIle Calls
F_OPEN		= $9a ;open file
F_CLOSE		= $9b ;close file
F_SYNC		= $9c ;sync file changes to disk
F_READ		= $9d ;read file
F_WRITE		= $9e ;write file
F_SEEK		= $9f ;set file position
F_FGETPO		= $a0 ;get file position
F_FSTAT		= $a1 ;get open file information
F_FTRUNC		= $a2 ;truncate/extend open file
F_OPENDI		= $a3 ;open directory for reading
F_READDI		= $a4 ;read directory entry
F_TELLDI		= $a5 ;get directory position
F_SEEKDI		= $a6 ;set directory position
F_REWIND		= $a7 ;rewind to start of directory
F_GETCWD		= $a8 ;get current working directory
F_CHDIR		= $a9 ;change directory
F_MKDIR		= $aa ;make directory
F_RMDIR		= $ab ;remove directory
F_STAT		= $ac ;get unopen file information
F_UNLINK		= $ad ;delete file
F_TRUNCA		= $ae ;truncate/extend unopen file
F_CHMOD		= $af ;change file attributes
F_RENAME		= $b0 ;rename/move file
F_GETFRE		= $b1 ;get free space

; esxDOS-compatible error codes
ESX_OK 		= 00 ;Unknown error
ESX_EOK 		= 10 ;OK
ESX_NONSENSE 	= 20 ;Nonsense in esxDOS
ESX_ESTEND 		= 30 ;Statement end error
ESX_EWRTYPE 	= 40 ;Wrong file type
ESX_ENOENT 		= 50 ;No such file or dir
ESX_EIO 		= 60 ;I/O error
ESX_EINVAL 		= 70 ;Invalid filename
ESX_EACCES 		= 80 ;Access denied
ESX_ENOSPC 		= 90 ;Drive full
ESX_ENXIO 		= 10 ;Invalid i/o request
ESX_ENODRV 		= 11 ;No such drive
ESX_ENFILE 		= 12 ;Too many files open
ESX_EBADF 		= 13 ;Bad file number
ESX_ENODEV 		= 14 ;No such device
ESX_EOVERFLOW 	= 15 ;File pointer overflow
ESX_EISDIR 		= 16 ;Is a directory
ESX_ENOTDIR 	= 17 ;Not a directory
ESX_EEXIST 		= 18 ;Already exists
ESX_EPATH 		= 19 ;Invalid path
ESX_ESYS 		= 20 ;Missing system
ESX_ENAMETOOLONG 	= 21 ;Path too long
ESX_ENOCMD 		= 22 ;No such command
ESX_EINUSE 		= 23 ;In use
ESX_ERDONLY 	= 24 ;Read only
ESX_EVERIFY 	= 25 ;Verify failed
ESX_ELOADINGKO 	= 26 ;Sys file load error
ESX_EDIRINUSE 	= 27 ;Directory in use
ESX_EMAPRAMACTIVE 	= 28 ;MAPRAM is active
ESX_EDRIVEBUSY 	= 29 ;Drive busy
ESX_EFSUNKNOWN 	= 30 ;Unknown filesystem
ESX_EDEVICEBUSY 	= 31 ;Device busy


ESX_SEEK_SET	= 0 ; set the fileposition to BCDE
ESX_SEEK_FWD	= 1 ; add BCDE to the fileposition
ESX_SEEK_BWD	= 2 ; subtract BCDE from the fileposition


macro esx_gethandle
	rst	8
	db	M_GETHANDLE
end

macro esx_getdate
	rst	8
	db	M_GETDATE
end

macro esx_seek	offset
	ld	bc,offset&0xffff0000>>16
	ld	de,offset&0xffff
	ld	l,ESX_SEEK_SET
	rst	8
	db	F_SEEK	
end

macro esx_read	addr,length
	ld	hl,addr
	ld	bc,length
	rst	8
	db	F_READ
end



#endif