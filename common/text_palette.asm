	db	%00000000,0
	db	%00000000,0 ; Black on Black
	db	%00000000,0
	db	%00000011,1	; Blue on Black
	db	%00000000,0
	db	%11100000,0	; Red on Black
	db	%00000000,0
	db	%11100111,0	; Magenta on Black
	db	%00000000,0
	db	%00011100,0	; Green on Black
	db	%00000000,0
	db	%00011111,1	; Cyan on Black
	db	%00000000,0
	db	%11111100,0	; Yellow on Black
	db	%00000000,0
	db	%11111111,1	; White on Black

	db	%00000011,1
	db	%00000000,0 ; Black on Blue
	db	%00000011,1
	db	%00000011,1	; Blue on Blue
	db	%00000011,1
	db	%11100000,0	; Red on Blue
	db	%00000011,1
	db	%11100111,0	; Magenta on Blue
	db	%00000011,1
	db	%00011100,0	; Green on Blue
	db	%00000011,1
	db	%00011111,1	; Cyan on Blue
	db	%00000011,1
	db	%11111100,0	; Yellow on Blue
	db	%00000011,1
	db	%11111111,1	; White on Blue

	db	%11100000,0
	db	%00000000,0 ; Black on Red
	db	%11100000,0
	db	%00000011,1	; Blue on Red
	db	%11100000,0
	db	%11100000,0	; Red on Red
	db	%11100000,0
	db	%11100111,0	; Magenta on Red
	db	%11100000,0
	db	%00011100,0	; Green on Red
	db	%11100000,0
	db	%00011111,1	; Cyan on Red
	db	%11100000,0
	db	%11111100,0	; Yellow on Red
	db	%11100000,0
	db	%11111111,1	; White on Red

	db	%11100111,0
	db	%00000000,0 ; Black on Magenta
	db	%11100111,0
	db	%00000011,1	; Blue on Magenta
	db	%11100111,0
	db	%11100000,0	; Red on Magenta
	db	%11100111,0
	db	%11100111,0	; Magenta on Magenta
	db	%11100111,0
	db	%00011100,0	; Green on Magenta
	db	%11100111,0
	db	%00011111,1	; Cyan on Magenta
	db	%11100111,0
	db	%11111100,0	; Yellow on Magenta
	db	%11100111,0
	db	%11111111,1	; White on Magenta

	db	%00011100,0
	db	%00000000,0 ; Black on Green
	db	%00011100,0
	db	%00000011,1	; Blue on Green
	db	%00011100,0
	db	%11100000,0	; Red on Green
	db	%00011100,0
	db	%11100111,0	; Magenta on Green
	db	%00011100,0
	db	%00011100,0	; Green on Green
	db	%00011100,0
	db	%00011111,1	; Cyan on Green
	db	%00011100,0
	db	%11111100,0	; Yellow on Green
	db	%00011100,0
	db	%11111111,1	; White on Green

	db	%00011111,1
	db	%00000000,0 ; Black on Cyan
	db	%00011111,1
	db	%00000011,1	; Blue on Cyan
	db	%00011111,1
	db	%11100000,0	; Red on Cyan
	db	%00011111,1
	db	%11100111,0	; Magenta on Cyan
	db	%00011111,1
	db	%00011100,0	; Green on Cyan
	db	%00011111,1
	db	%00011111,1	; Cyan on Cyan
	db	%00011111,1
	db	%11111100,0	; Yellow on Cyan
	db	%00011111,1
	db	%11111111,1	; White on Cyan

	db	%11111100,0	
	db	%00000000,0 ; Black on Yellow
	db	%11111100,0	
	db	%00000011,1	; Blue on Yellow
	db	%11111100,0	
	db	%11100000,0	; Red on Yellow
	db	%11111100,0	
	db	%11100111,0	; Magenta on Yellow
	db	%11111100,0	
	db	%00011100,0	; Green on Yellow
	db	%11111100,0	
	db	%00011111,1	; Cyan on Yellow
	db	%11111100,0	
	db	%11111100,0	; Yellow on Yellow
	db	%11111100,0	
	db	%11111111,1	; White on Yellow

	db	%11111111,1
	db	%00000000,0 ; Black on White
	db	%11111111,1
	db	%00000011,1	; Blue on White
	db	%11111111,1
	db	%11100000,0	; Red on White
	db	%11111111,1
	db	%11100111,0	; Magenta on White
	db	%11111111,1
	db	%00011100,0	; Green on White
	db	%11111111,1
	db	%00011111,1	; Cyan on White
	db	%11111111,1
	db	%11111100,0	; Yellow on White
	db	%11111111,1
	db	%11111111,1	; White on White
