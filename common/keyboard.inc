#ifndef keyboard_inc
keyboard_inc = 1
                   
KEY_EDIT 		= $01
KEY_CAPS 		= $02
KEY_TRUEVIDEO 	= $03     
KEY_INVVIDEO	= $04
KEY_LEFT		= $05
KEY_DOWN		= $06
KEY_UP		= $07
KEY_RIGHT		= $08
KEY_GRAPH		= $09
KEY_DELETE		= $0A
KEY_PAGE_DOWN	= $0B
KEY_PAGE_UP		= $0C
KEY_ENTER		= $0D
KEY_BREAK		= $1B
KEY_SYM_SPACE	= $1C
KEY_CAPS_ENTER	= $1d
KEY_SYM_ENTER	= $1E

KEY_EXT		= $80

#endif
