; Heaviliy modified version of https://github.com/next-dev/ned/blob/master/src/keyboard.asm
; ToDo: rewrite/extend to support key repeat/delay

	include	"keyboard.inc"

KEYBOARD_BUFFER = $80

;-----------------------------------------------------------------------------
; Scans the keyboard and stores the state
; Output:
;   HL = pointer to keyscan
;-----------------------------------------------------------------------------
scan_keys	ld	hl,$8000
	ld	(hl),0
	ld	hl,key_store
	ld	bc,$fdfe
	push	hl
	ld	e,8
.loop	ld	d,(hl)
	in	a,(c)
	cpl
	and	$1f
	ld	(hl),a
	inc	hl
	ld	(hl),d
	inc	hl
	rlc	b
	dec	e
	jr	nz,.loop
	pop	hl

	ld	ix,key_store
	xor	a
	bit	0,(ix+14)	; Test for Caps (row 7 * 2 bytes)
	jr	z,.no_caps
	or	40

.no_caps	bit	1,(ix+12)	; Test for symbol shift (row 6 * 2 bytes)
	jr	z,.no_sym
	or	80

.no_sym	ex	de,hl
	ld	hl,key_table
	add	hl,a


	ld	b,8
.row	ld	a,(de)
	inc	de
	ld	c,a
	ld	a,(de)
	inc	de
	xor	$ff
	and	c
	push	hl
.col	and	a
	jr	z,.end_row

	srl	a
	jr	nc,.ignore

	ld	c,(hl)
	inc	c
	jr	z,.ignore
	dec	c
	push	bc
	ld	b,KEYBOARD_BUFFER
	call	buffer_append
	pop	bc
.ignore	inc	hl
	jr	.col

.end_row	ld	a,5
	pop	hl
	add	hl,a
	djnz	.row

	ret

buffer_append
	push	hl
	ld	hl,$8000
	inc	(hl)
	ld	l,(hl)
	ld	(hl),c
	pop	hl
	ret


buffer_take	push	hl
	ld	hl,$8000
	ld	a,(hl)
	and	a
	jr	z,.empty
	dec	(hl)
	ld	l,(hl)
	inc	hl
	ld	a,(hl)
.empty	pop     	hl
	and	a
	ret

key_store	ds	16,0

key_table	; Unshifted
	db	'a','s','d','f','g'                             ; A-G
	db	'q','w','e','r','t'                             ; Q-T
	db	'1','2','3','4','5'                             ; 1-5
	db	'0','9','8','7','6'                             ; 0-6
	db	'p','o','i','u','y'                             ; P-Y
	db	KEY_ENTER,'l','k','j','h'                             ; Enter-H
	db	' ',$ff,'m','n','b'                             ; Space-B
	db	$ff,'z','x','c','v'                             ; Caps-V

	; CAPS shifted
	db	'A','S','D','F','G'                             ; A-G
	db	'Q','W','E','R','T'                             ; Q-T
	db	KEY_EDIT,KEY_CAPS,KEY_TRUEVIDEO,KEY_INVVIDEO,KEY_LEFT                             ; 1-5
	db	KEY_DELETE,KEY_GRAPH,KEY_RIGHT,KEY_UP,KEY_DOWN                             ; 0-6
	db	'P','O','I','U','Y'                             ; P-Y
	db	KEY_CAPS_ENTER,'L','K','J','H'                             ; Enter-H
	db	KEY_BREAK,$ff,'M','N','B'                             ; Space-B
	db	$ff,'Z','X','C','V'                             ; Caps-V

	; SYM shifted
	db	'~','|',39,'{','}'                            ; A-G
	db	$7f,'w','e','<','>'                             ; Q-T
	db	'!','@','#','$','%'                             ; 1-5
	db	'_',')','(',$27,'&'                             ; 0-6
	db	$22,';','i',']','['                             ; P-Y
	db	KEY_SYM_ENTER,'=','+','-','^'                             ; Enter-H
	db	KEY_SYM_SPACE,$ff,'.',',','*'                             ; Space-B
	db	$ff,':','`','?','/'                             ; Caps-V

	; EXT shifted
	db	'a'+$80,'s'+$80,'d'+$80,'f'+$80,'g'+$80         ; A-G
	db	'q'+$80,'w'+$80,'e'+$80,'r'+$80,'t'+$80         ; Q-T
	db	'1'+$80,'2'+$80,'3'+$80,'4'+$80,'5'+$80         ; 1-5
	db	'0'+$80,'9'+$80,'8'+$80,KEY_PAGE_UP,KEY_PAGE_DOWN         ; 0-6
	db	'p'+$80,'o'+$80,'i'+$80,'u'+$80,'y'+$80         ; P-Y
	db	$8d,    'l'+$80,'k'+$80,'j'+$80,'h'+$80         ; Enter-H
	db	$a0,    $ff,    'm'+$80,'n'+$80,'b'+$80         ; Space-B
	db	$ff,    'z'+$80,'x'+$80,'c'+$80,'v'+$80         ; Caps-V
