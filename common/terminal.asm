	include	"nextreg.inc"

;-----------------------------------------------------------------------------
; Intialiase text mode
;-----------------------------------------------------------------------------
term_init	nextreg	NR_TILEMAP_CONTROL,%11001011 	; init text mode
	nextreg	NR_ENHANCED_ULA_CONTROL,%00110001 	; Enable ULA Next, set palette TM 1
	nextreg	NR_TILEMAP_BASE_ADDRESS,0	; tilemap base address
	nextreg	NR_TILE_DEFINITIONS_BASE_ADDRESS,20	; tilemap definitions	
	ld	hl,16384
	ld	de,16385
	ld	bc,0+(160*32)-1
	ld	(hl),l
	ldir
	ret

;-----------------------------------------------------------------------------
; Set current colour
; A = Foreground
; E = Bakcground
;-----------------------------------------------------------------------------

term_set_colour
	add	a
	ld	d,16
	mul	de
	or	e
	ld	(text_attr),a
	ret

;-----------------------------------------------------------------------------
; Moves the cursor
; E = Row
; A = Column
;-----------------------------------------------------------------------------

term_move	;break
	ld	hl,0x4000
	ld	d,160
	mul	de
	add	hl,de
	add	hl,a
	add	hl,a
	ld	(cursor_addr),hl
	ret


;-----------------------------------------------------------------------------
; Print text to the terminal
; HL = Pointer to string
;-----------------------------------------------------------------------------
term_print	push	.print_exit
	ld	de,(cursor_addr)
	ld	a,(text_attr)
.loop	ex	af,af'
	ld	a,(hl)
	and	a
	ret	z

	ldi
	ex	af,af'
	ld	(de),a
	inc	de
	jr	.loop

.print_exit	;break
	ld	(cursor_addr),de
	ret

cursor_addr		dw	0x4000
text_attr		db	14
