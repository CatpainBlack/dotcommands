	include	"p3dos.inc"
	include	"nextreg.inc"

;-----------------------------------------------------------------------------
; Allocate n pages
; DE = address to store page numbers
; B  = count
;-----------------------------------------------------------------------------
allocate_banks
.loop	push	bc
	push	de
	ld	h,RC_BANKTYPE_ZX
	ld	l,RC_BANK_ALLOC
	exx
	ld	de,IDE_BANK
	ld	c,7
	rst	8
	db	M_P3DOS
	ld	a,e
	pop	de
	ld	(de),a
	inc	de
	pop	bc
	djnz	.loop
	ret

;-----------------------------------------------------------------------------
; Free n pages
; DE = address of page numbers table
; B  = count
;-----------------------------------------------------------------------------
free_banks	
.loop	push	bc
	push	de	
	ld	h,RC_BANKTYPE_ZX
	ld	l,RC_BANK_FREE
	ld	a,(de)
	ld	e,a
	exx
	ld	de,IDE_BANK
	ld	c,7
	rst	8
	db	M_P3DOS
	pop	de
	inc	de
	pop	bc
	djnz	.loop
	ret

;-----------------------------------------------------------------------------
; Copy data from bank b to a
; A = Destination Bank
; B = Source Bank
;-----------------------------------------------------------------------------
copy_bank	nextreg	NR_MMU5,a
	ld	a,b
	nextreg	NR_MMU4,a
	ld	hl,32768
	ld	de,32768+8192
	ld	bc,8192
	ldir
	nextreg	NR_MMU4,4
	nextreg	NR_MMU5,5
	ret








