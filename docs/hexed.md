### Hexed

A simple hexeditor to view and edit memory contents

Commandline

* -pnn	page number in hex
* -annnn  start address in page

Keys

* Cursor keys to move
* Ext-Q		Quit
* Ext-Right	End of line
* Ext-Left	Beginning of line
* Ext-Up		Page Up
* Ext-Down	Page Down