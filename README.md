## A collection of dotcommands for the ZX Spectrum Next

### Hexed

A simple hexeditor to view and edit memory contents

Keys

* Cursor keys to move
* Ext-Q		Quit
* Ext-Right	End of line
* Ext-Left	Beginning of line
* Ext-Up		Page Up
* Ext-Down	Page Down