;-----------------------------------------------------------------------------
; Parse the commandline arguments
;-----------------------------------------------------------------------------
parse_args	ld	a,l
	or	h
	ret	z	; no arguments, exit

	ld	(error+1),sp
.next_switch
	call	find_switch
	jr	c,.found_switch

	ld	hl,(addr)
	ld	a,(page)
	and	a
	ret

.found_switch
	cp	'p'
	call	z,collect_page
	cp	'a'
	call	z,collect_address
	jr	.next_switch

;-----------------------------------------------------------------------------
; Reads the page number
;-----------------------------------------------------------------------------
collect_page
	ex	af,af'
	call	collect_hex_byte
	ld	(page),a
	ex	af,af'
	ret

;-----------------------------------------------------------------------------
; Reads the address (0000h-2000h)
;-----------------------------------------------------------------------------
collect_address
	ex	af,af'
	ld	de,0
	call	collect_hex_byte
	ld	d,a
	call	collect_hex_byte
	ld	e,a
	ld	(addr),de
	ex	af,af'
	ret

;-----------------------------------------------------------------------------
; Reads a hex number from the commandline
;-----------------------------------------------------------------------------
collect_hex_byte
	call	ascii_to_nybble
	swapnib
	ld	c,a
	call	ascii_to_nybble
	or	c
	ret

;-----------------------------------------------------------------------------
; Converts an ascii character at (HL) to a number
;-----------------------------------------------------------------------------
ascii_to_nybble
	ld	a,(hl)
	inc	hl
	cp	'Z'
	jr	c,.lower_case
	and	%11011111
.lower_case	sub	'0'
	cp	$a
	jr	c,.not_number
	sub	7
.not_number	cp	$10
	jr	nc,error
	ret


;-----------------------------------------------------------------------------
; Search the commandline for the first occurance of a switch
; 	HL = Command Line Buffer
; Returns
; 	Carry set if found
; 	HL = address of character following '-'
;-----------------------------------------------------------------------------
find_switch
	ld	a,(hl)
	cp	'-'
	jr	z,.found
	cp	':'
	ret	z
	cp	$d
	ret	z
	inc	hl
	jr	find_switch

.found	inc	hl
	ld	a,(hl)
	inc	hl
	scf
	ret

error:	ld	sp,0
	xor	a
	ld	hl,bad_command
	scf
	ret

bad_command	db	"Invalid hex numbe",'r'+$80

page	db	0
addr	dw	0