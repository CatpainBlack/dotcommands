FIRST_ROW 	= $4000+320
SECOND_ROW	= FIRST_ROW+160
SCROLL_AREA	= 160*27
LAST_ROW	= $4000+(29*160)
LAST_COLUMN = 57
FIRST_COLUMN= 12
BOTTOM_ROW  = 29
TOP_ROW	= 2
PAGE_SIZE	= 16*28

BG_COLOUR	= 28

;-----------------------------------------------------------------------------
; Hex editor control event handler
;-----------------------------------------------------------------------------
hex_edit	push	af
	cp	'0'
	jp	c,.not_hex_digit
	cp	'9'+1
	jp	c,enter_hex
	cp	'a'
	jr	c,.not_hex_digit	
	cp	'g'
	jp	c,enter_hex
.not_hex_digit	
	cp	KEY_RIGHT
	call	z,move_cursor_tile_address_right
	cp	KEY_LEFT
	call	z,move_cursor_tile_address_left
	cp	KEY_DOWN
	call	z,move_cursor_tile_address_down
	cp	KEY_UP
	call	z,move_cursor_tile_address_up
	cp	KEY_EXT+'5'
	call	z,move_line_start
	cp	KEY_EXT+'8'
	call	z,move_line_end
	cp	KEY_PAGE_DOWN
	call	z,page_down
	cp	KEY_PAGE_UP
	call	z,page_up
	pop	af
	ret		

enter_hex	sub	'0'
	cp	10
	jr	c,.not_alpha
	sub	39
.not_alpha	ld	c,a
	ld	de,(edit_address)
	ld	a,(edit_nybble)
	and	a
	jr	z,.top_nibble

	ld	a,(de)
	and	0xF0
	jr	.store

.top_nibble	ld	a,c
	swapnib
	ld	c,a
	ld	a,(de)
	and	0x0F
.store	or	c

	ld	(de),a
	ex	af,af'
	pop	af
	ld	c,a
	cp	'9'+1
	jr	c,.numeric
	and	$df
.numeric	ld	hl,(cursor_tile_address)
	dec	hl
	ld	(hl),a

	ld	hl,0x4000
	ld	a,(cursor_position)
	ld	e,a
	ld	d,160
	mul	de
	add	hl,de
	ld	a,(edit_address)
	and	15
	add	a
	add	124
	add	hl,a
	ex	af,af'
	ld	(hl),a

	jp	move_cursor_tile_address_right


;-----------------------------------------------------------------------------
; Page up
;-----------------------------------------------------------------------------
page_up
	ld	de,PAGE_SIZE
	call	decrement_addr
	jr	redraw

;-----------------------------------------------------------------------------
; Page Down
;-----------------------------------------------------------------------------
page_down	ld	de,PAGE_SIZE
	call	increment_addr


redraw	ld	hl,(edit_address)
	ld	a,(edit_page)
	push	hl
	push	af
	ld	hl,FIRST_ROW
	call	full_refresh
	pop	af
	pop	hl
	ld	(edit_address),hl
	ld	(edit_page),a
	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address to the end of the current line
;-----------------------------------------------------------------------------
move_line_end:
	ld	a,LAST_COLUMN
	ld	(cursor_position+1),a
	xor	a
	ld	(edit_nybble),a
	ld	a,(edit_address)
	or	$f
	and	-1
	ld	(edit_address),a
	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address to the start of the current line
;-----------------------------------------------------------------------------
move_line_start:
	ld	a,FIRST_COLUMN
	ld	(cursor_position+1),a
	xor	a
	ld	(edit_nybble),a
	ld	a,(edit_address)
	and	$f0
	ld	(edit_address),a
	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address down
;-----------------------------------------------------------------------------
move_cursor_tile_address_down:
	ld	a,(cursor_position)
	cp	BOTTOM_ROW
	jp	z,scroll_up
	inc	a
	ld	(cursor_position),a
	ld	de,16
	call	increment_addr
	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address Up
;-----------------------------------------------------------------------------	
move_cursor_tile_address_up:
	ld	a,(cursor_position)
	cp	2
	jp	z,scroll_down
	dec	a
	ld	(cursor_position),a
	ld	de,16
	call	decrement_addr
	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address Right
;-----------------------------------------------------------------------------	
move_cursor_tile_address_right:
	ld	hl,cursor_position+1
	ld	a,(hl)
	cp	58
	ret	z
	inc	(hl)
	ld	a,(edit_nybble)
	and	a
	jr	z,.dont_inc_addr

	ex	af,af'
	inc	(hl)
	ld	de,1
	call	increment_addr
	ex	af,af'

.dont_inc_addr
	xor	1
	ld	(edit_nybble),a

	jp	update_cursor_tile_address_pos

;-----------------------------------------------------------------------------
; Moves the cursor_tile_address Left
;-----------------------------------------------------------------------------
move_cursor_tile_address_left:
	ld	hl,cursor_position+1
	ld	a,(hl)
	cp	12
	ret	z
	dec	(hl)
	ld	a,(edit_nybble)
	and	a
	jr	nz,.dont_dec_addr

	ex	af,af'
	dec	(hl)
	ld	de,1
	call	decrement_addr
	ex	af,af'

.dont_dec_addr
	xor	1
	ld	(edit_nybble),a

;-----------------------------------------------------------------------------
; Updates the cursor_tile_address position
;-----------------------------------------------------------------------------
update_cursor_tile_address_pos
	ld	hl,(cursor_tile_address)
	ld	(hl),BG_COLOUR
	ld	de,(cursor_position)
	ld	a,d
	ld	hl,0x4000
	ld	d,160
	mul	de
	add	hl,de
	add	hl,a
	add	hl,a
	inc	hl
	ld	(cursor_tile_address),hl
	ld	(hl),16*6

	ld	hl,$4000+160+130
	ld	a,(edit_page)
	call	disp_hex
	ld	a,12
	add	hl,a
	ld	a,(edit_address+1)
	call	disp_hex
	ld	a,(edit_address)

;-----------------------------------------------------------------------------
; Convert A to hex digits and store at the screen address in HL
;-----------------------------------------------------------------------------
disp_hex	push	de
	ld	c,a
	and  	15
    	add  	a,$90
    	daa
    	adc  	a,$40
    	daa
    	ld	e,a
    	ld	a,c
    	swapnib
	and  	15
    	add  	a,$90
    	daa
    	adc  	a,$40
    	daa
	ld	(hl),a
	inc	hl
	inc	hl
	ld	(hl),e
	inc	hl
	inc	hl
	pop	de
    	ret

;-----------------------------------------------------------------------------
; Scrolls the window up
;-----------------------------------------------------------------------------
scroll_up:	ld	hl,(cursor_tile_address)
	ld	(hl),BG_COLOUR
	ld	de,FIRST_ROW
	ld	hl,SECOND_ROW
	ld	bc,SCROLL_AREA
	ldir
	ld	hl,(edit_address)
	ld	a,16
	add	hl,a
	ld	a,h
	cp	$20
	jr	z,.at_end

.update	ld	(edit_address),hl
	ld	hl,LAST_ROW
	call	update_row
	jp	update_cursor_tile_address_pos
	
.at_end	ld	hl,edit_page
	inc	(hl)
	ld	hl,0
	jr	.update

;-----------------------------------------------------------------------------
; Scrolls the window down
;-----------------------------------------------------------------------------
scroll_down:
	ld	hl,(cursor_tile_address)
	ld	(hl),BG_COLOUR
	ld	hl,LAST_ROW-1
	ld	de,LAST_ROW+159
	ld	bc,SCROLL_AREA
	lddr
	ld	hl,(edit_address)
	ld	a,h
	or	l
	jr	z,.at_start
	ld	de,-16
	add	hl,de
.update	ld	(edit_address),hl
	ld	hl,FIRST_ROW
	call	update_row
	jp	update_cursor_tile_address_pos

.at_start	ld	a,(edit_page)
	dec	a
	ld	(edit_page),a
	ld	hl,8192-16
	ld	(edit_address),hl
	jr	.update

;-----------------------------------------------------------------------------
; Display a row
; HL = screen address
;-----------------------------------------------------------------------------
update_row

	ld	a,2
	ld	(hl),$b3
	add	hl,a
	ld	(hl),' '
	add	hl,a
	ld	a,(edit_page)
	nextreg	NR_MMU6,a
	call	disp_hex

	ld	(hl),':'
	inc	hl
	inc	hl

	ld	a,(edit_address+1)
	ld	d,a
	call	disp_hex
	ld	a,(edit_address)
	and	-16
	ld	e,a
	call	disp_hex

	ld	a,d
	add	$C0
	ld	d,a	

	push	de

	ld	a,2
	ld	(hl),' '
	add	hl,a
	ld	(hl),$b3
	add	hl,a
	ld	(hl),' '
	add	hl,a

	ld	b,16
.loop	push	bc
	ld	a,(de)
	call	disp_hex
	inc	de
	inc	hl
	inc	hl

	pop	bc
	djnz	.loop

	ld	a,2
	ld	(hl),$b3
	add	hl,a
	ld	(hl),' '
	add	hl,a

	pop	de
	ex	hl,de
	ld	b,16
.copy	ld	a,(hl)
	ld	(de),a
	inc	hl
	inc	de
	inc	de
	djnz	.copy

	ex	hl,de
	ld	(hl),' '
	inc	hl
	inc	hl
	ld	(hl),$b3

	ld	a,(page6)
	nextreg	NR_MMU6,a
	ret

;-----------------------------------------------------------------------------
; Decrement the current address
; DE = Amount to decrement by
;-----------------------------------------------------------------------------
decrement_addr
	ld	hl,(edit_address)
	xor	a
	sbc	hl,de
	jr	c,.beginning_of_bank
.store_addr	ld	(edit_address),hl
	ret

.beginning_of_bank
	ld	hl,edit_page
	dec	(hl)
	ld	hl,$1ff0
	jr	.store_addr

;-----------------------------------------------------------------------------
; Increment the current address
; DE = Amount to increment by
;-----------------------------------------------------------------------------
increment_addr
	ld	hl,(edit_address)
	add	hl,de
	ld	a,h
	cp	$1f
	jr	nc,.end_of_bank
.store_addr	ld	(edit_address),hl
	ret

.end_of_bank
	ld	hl,edit_page
	inc	(hl)
	ld	hl,0
	jr	.store_addr


;-----------------------------------------------------------------------------
; Setup the editor, display the initial screen
; HL = Bank offset; A = Bank
;-----------------------------------------------------------------------------
setup_editor
	push	af
	ld	(edit_page),a
	ld	a,l
	and	$f0
	ld	l,a
	ld	(edit_address),hl
	push	hl


	ld	bc,TBBLUE_REGISTER_SELECT
	ld	a,NR_MMU6
	out	(c),a
	inc	b
	in	a,(c)
	ld	(page6),a

	ld	hl,FIRST_ROW
	push	hl
	ld	(hl),' '
	inc	hl
	ld	(hl),BG_COLOUR
	dec	hl
	ld	de,FIRST_ROW+2
	ld	bc,SCROLL_AREA+158
	ldir
	pop	hl
	
	call	full_refresh

	pop	hl
	pop	af
	ld	(edit_address),hl
	ld	(edit_page),a
	jp	update_cursor_tile_address_pos

full_refresh
	ld	b,28
.next_row	push	bc
	push	hl
	call	update_row
	ld	de,16
	call	increment_addr
	pop	hl
	ld	a,160
	add	hl,a
	pop	bc
	djnz	.next_row
	ret

page6		db	0
cursor_position	db	2,12
edit_page		db	10
edit_address	dw	0
edit_nybble		db	0
cursor_tile_address	dw	$4159

