	org	0x2000

	include 	"esxdos.inc"
	include	"p3dos.inc"
	include	"nextreg.inc"
	include	"ports.inc"
	include	"keyboard.inc"
	include	"terminal.inc"

	call	parse_args
	ret	c

	push	hl
	push	af
	call	setup

	di

	call	draw_screen
	ld	hl,32768
	ld	de,32769
	ld	(hl),l
	ld	bc,8191
	ldir

	pop	af
	pop	hl
	call	setup_editor

.forever	ei:halt:di

.no_mouse	call	scan_keys
	call	buffer_take
	call	hex_edit
	cp	KEY_EXT+'q'
	jr	nz,.forever

;-----------------------------------------------------------------------------
; Restore the system state
;-----------------------------------------------------------------------------
cleanup	di
	ld	a,(bank_store)
	ld	b,a
	ld	a,10
	call	copy_bank
	ld	a,(bank_store+1)
	ld	b,a
	ld	a,11
	call	copy_bank
	nextreg	NR_TILEMAP_CONTROL,0
	ei

	nextreg	NR_MMU4,4

	ld	de,bank_store
	ld	b,3
	call	free_banks

	ld	a,(turbo_store)
	nextreg	NR_TURBO_CONTROL,a
	xor	a
	ret

;-----------------------------------------------------------------------------
; Render the titlebar and status bar
;-----------------------------------------------------------------------------
draw_screen:
	ld	a,BLACK
	ld	e,WHITE
	call	term_set_colour
	ld	e,0
	ld	a,0
	call	term_move
	ld	hl,title_bar
	call	term_print	
	ld	e,31
	ld	a,0
	call	term_move
	ld	hl,status_bar
	call	term_print

	ld	e,1
	ld	a,0
	call	term_move
	ld	a,YELLOW
	ld	e,BLUE
	call	term_set_colour
	ld	hl,top_row
	call	term_print
	ld	a,WHITE
	ld	e,BLUE
	call	term_set_colour
	ld	hl,addr_info
	call	term_print
	ld	a,YELLOW
	ld	e,BLUE
	call	term_set_colour
	ld	hl,top_row_2
	call	term_print

	ld	e,30
	ld	a,0
	call	term_move
	ld	hl,bottom_row
	call	term_print
	ret

;-----------------------------------------------------------------------------
; Save the current system state and setup tilemap mode.
;-----------------------------------------------------------------------------
setup:	ld	bc,TBBLUE_REGISTER_SELECT
	ld	a,NR_TURBO_CONTROL
	out	(c),a
	inc	b
	in	a,(c)
	swapnib
	ld	(turbo_store),a		; store the current CPU speed setting
	nextreg	NR_TURBO_CONTROL,CPU_SPD_28	; set cpu speed to 28mhz

	esx_gethandle			; get a file handle to the dot command
	ld	(dot_handle),a		; save it
	ld	de,bank_store
	ld	b,3
	call	allocate_banks		; allocate 2x8k banks

	ld	a,(bank_store)
	ld	b,10
	call	copy_bank
	ld	a,(bank_store+1)
	ld	b,11
	call	copy_bank

	call	term_init
	ld	a,(work_buffer)
	nextreg	NR_MMU4,a

	ld	a,(dot_handle)
	esx_seek	8192
	jr	c,.init_error		; exit if error

	ld	a,(dot_handle)
	esx_read	0x5400,sizeof(font)
	jr	c,.init_error

	ld	a,(dot_handle)
	esx_read	0x8000,palette_length
	jr	c,.init_error

        	nextreg 	NR_ENHANCED_ULA_CONTROL,%00110000   ; tilemap pal0
        	nextreg 	NR_PALETTE_INDEX,0
	ld	hl,0x8000
	ld      	b,palette_length
.loop	ld      	a,(hl)
	inc     	hl
	nextreg 	NR_ENHANCED_ULA_PALETTE_EXTENSION,a
	djnz    	.loop
	ret

.init_error	break
	call	cleanup
	pop	hl
	ld	a,ESX_EIO
	scf
	ret



	include	"memory.asm"
	include	"terminal.asm"
	include	"keyboard.asm"
	include	"hexedit.asm"
	include	"args.asm"

;-----------------------------------------------------------------------------
; Data
;-----------------------------------------------------------------------------
dot_handle	db	0
turbo_store	db	0
bank_store	db	0,0
work_buffer	db	0
;bank	db	1
;addr	dw	$4000



                        ;--------------------------------------------------------------------------------;
title_bar	db	" -=[ HexEd 0.1 ]=-                                                              ",0
status_bar	db	"                                                                                ",0
		;----------------------------------------------------------------------------------------------------------------------------------------------------------------
top_row	dh	"D5CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDB500"
addr_info	db	" Page:-- Addr:---- ",0
top_row_2	dh	"C6B800"
bottom_row	dh	"C0C4C4C4C4C4C4C4C4C4C1C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C1C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4D900"


;-----------------------------------------------------------------------------
; Data (Padded 8k)
;-----------------------------------------------------------------------------	
	include	"data.asm"